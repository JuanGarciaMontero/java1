//============================================================================
// Name        : Hola_Mundo_C++.cpp
// Author      : Juan
// Version     :
// Copyright   : Juan
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Hola Juan" << endl; // prints Hola Juan
	return 0;
}
