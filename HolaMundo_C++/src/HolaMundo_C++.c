/*
 ============================================================================
 Name        : HolaMundo_C++.c
 Author      : Juan
 Version     :
 Copyright   : Juan
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	puts("Hola Juan"); /* prints Hola Juan */
	return EXIT_SUCCESS;
}
